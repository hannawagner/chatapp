import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './style.scss';

import Home from './pages/home.component.jsx'
import Character from './pages/character.component'
import Location from './pages/location.component'
import Chat from './pages/chat.component.jsx'


render(

    <Router>
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="/">Chat App</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li id="home" className="nav-item">
                            <a className="nav-link" href="/">Home</a>
                        </li>
                        <li id="character" className="nav-item">
                            <a className="nav-link" href="/character">Character</a>
                        </li>
                        <li id="location" className="nav-item">
                            <a className="nav-link" href="/location">Location</a>
                        </li>
                        <li id="chat" className="nav-item">
                            <a className="nav-link" href="/chat">Chat</a>
                        </li>
                    </ul>
                </div>
            </nav>

            <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/character' component={Character} />
                <Route exact path='/location' component={Location} />
                <Route exact path='/chat' component={Chat} />
            </Switch>
        </div>
    </Router>,


    document.getElementById('root')
);
