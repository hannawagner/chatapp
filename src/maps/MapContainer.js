import React, {Component} from 'react';
import ReactDOM from 'react-dom'

export default class MapContainer extends Component {

    componentDidUpdate() {
        this.loadMap();
    }

    loadMap() {
        if (this.props && this.props.google) {

            const {google} = this.props;
            const maps = google.maps;

            const mapRef = this.refs.map;
            const node = ReactDOM.findDOMNode(mapRef);

            let location = this.props.position.location;
            let lat = parseFloat(this.props.position.lat);
            let lng = parseFloat(this.props.position.lng);

            const mapConfig = Object.assign({}, {
                center: {lat: lat, lng: lng},
                zoom: 11,
                mapTypeId: 'roadmap'
            });

            this.map = new maps.Map(node, mapConfig);


            const marker = new google.maps.Marker({
                position: {lat: lat, lng: lng},
                map: this.map,
                title: location
            });


        }
    }

    render() {
        return (
            <div id="map" ref="map">
                loading map...
            </div>
        )
    }
}