import React, { Component } from 'react';

class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: ''
        };
    }

    handleSubmit(username){
        //save to localstorage
        localStorage.setItem("username", username);
    }

    render(){
        return (
            <div className="container">
                <div className="row">
                    <div className="col-12">

                        <div className="card">
                            <div className="card-body">
                                <h2 className="card-title">Enter your Username</h2>
                                <hr/>
                                <div>Current Username: {this.state.username} </div>
                            </div>
                            <div className="card-footer">
                                <input type="text" placeholder="Username" value={this.state.username} onChange={ev => this.setState({username: ev.target.value})} className="form-control"/>
                                <br/>
                                <a href="./character"> <button onClick={this.handleSubmit(this.state.username)} className="btn btn-primary form-control">Send
                                </button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home