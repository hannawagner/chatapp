import React, { Component } from 'react';
import io from "socket.io-client";

class Chat extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            character: '',
            location: '',
            message: '',
            messages: []
        };

        this.socket = io('localhost:5000');

        this.socket.on('RECEIVE_MESSAGE', function(data){
            addMessage(data);
        });

        const addMessage = data => {
            console.log(data);
            this.setState({messages: [...this.state.messages, data]});
            console.log(this.state.messages);
        };

        this.sendMessage = ev => {
            ev.preventDefault();
            this.socket.emit('SEND_MESSAGE', {
                author: this.state.username,
                message: this.state.message,
                location: this.state.location,
                character: this.state.character
            });
            this.setState({message: ''});
        }
    }

    componentDidMount(){
        let historyName = localStorage.getItem("username");
        let historyCharacter = localStorage.getItem("character");
        let historyLocation = localStorage.getItem("location");

        this.setState({username: historyName});
        this.setState({character: historyCharacter});
        this.setState({location: historyLocation});
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <div className="card-title">Global Chat</div>
                                <hr/>


                                <div className="message-list">
                                    {this.state.messages.map(message => {
                                        return (
                                                <div className="message-wrapper">
                                                    <div className="part">
                                                        <img id="characterImage" className="mini-image" alt="characterImage"  src={message.character}/>
                                                    </div>
                                                    <div className="part">
                                                        <p><b>{message.author} </b> from <em> {message.location} </em> said:&nbsp;</p>
                                                    </div>
                                                    <div className="part">
                                                        <p>{message.message}</p>
                                                    </div>
                                                </div>
                                        )
                                    })}
                                </div>

                            </div>
                            <div className="card-footer">
                                <input type="text" placeholder="Message" className="form-control"
                                       value={this.state.message}
                                       onChange={ev => this.setState({message: ev.target.value})}/>
                                <br/>
                                <button onClick={this.sendMessage} className="btn btn-primary form-control">Send
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Chat