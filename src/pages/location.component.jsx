import React, {Component} from 'react';

import {GoogleApiWrapper} from 'google-maps-react'
import MapContainer from '../maps/MapContainer'

class Location extends Component {

    constructor(props) {

        super(props);

        this.state = {
            location: 'Linz',
            lng: '14.40908',
            lat: '48.30694',
        };
    }

    handleSubmit() {
        //save to localstorage
        localStorage.setItem("location", this.state.location);
    }

    getLocation(location) {
        //only consider locations with length 2 or more

        if (location.length > 1) {
            fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + location + '&key=AIzaSyC03PEuLHSxaDwJvWSfJVoptmSX3RjAU5w')
                .then(results => {
                    return results.json();
                }).then(data => {
                let innerLocation = data.results.map((result) => {
                    return ({result})
                });

                if (innerLocation[0]) {
                    let markerLocation = innerLocation[0].result.geometry.location;
                    let lat = markerLocation.lat;
                    let lng = markerLocation.lng;

                    if (lat !== this.state.lat) {
                        this.setState({lat: lat});
                    }
                    if (lng !== this.state.lng) {
                        this.setState({lng: lng});
                    }
                }

            })
        }
    }


    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <h2 className="card-title">Enter your Location</h2>
                                <hr/>
                                <div>Current Location: {this.state.location} </div>
                            </div>
                            <div className="card-footer">

                                <MapContainer google={this.props.google} position={this.state}/>

                                <input type="text" placeholder="Location" value={this.state.location}
                                       onChange={ev => this.setState({location: ev.target.value})}
                                       className="form-control"/>
                                <button className="location-btn" onClick={this.getLocation(this.state.location)}/>
                                <br/>
                                <a href="./chat">
                                    <button onClick={this.handleSubmit(this.state.location)}
                                            className="btn btn-primary form-control">Send
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        );

    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyC03PEuLHSxaDwJvWSfJVoptmSX3RjAU5w',
})(Location)