import React, { Component } from 'react';
import Pingu from '../imgs/Pingu.jpg'
import Panda from '../imgs/Panda.jpg'

class Character extends Component {


    constructor(props) {
        super(props);

        this.state = {
            character: ''
        };

        this.handleState = this.handleState.bind(this);
        this.handleState2 = this.handleState2.bind(this);

    }


    handleState(){
        let element = document.getElementById("pingu");
        element.classList.add("selected");

        let other = document.getElementById("panda");
        other.classList.remove("selected");

        let character = document.getElementById("characterImage");
        let ImageObject = {Pingu};
        let ImageSrc = (Object.values(ImageObject)[0]);
        character.src = "http://localhost:3000"+ImageSrc;

        this.setState({character: character.src});
        localStorage.setItem("character", character.src);
    }

    handleState2(){
        let element = document.getElementById("panda");
        element.classList.add("selected");

        let other = document.getElementById("pingu");
        other.classList.remove("selected");

        let character = document.getElementById("characterImage");
        let ImageObject = {Panda};
        let ImageSrc = (Object.values(ImageObject)[0]);
        character.src = "http://localhost:3000"+ImageSrc;

        this.setState({character: character.src});
        localStorage.setItem("character", character.src);
    }

    render(){
        return (
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <h2 className="card-title">Choose your Character</h2>
                                <hr/>
                                <div>Current Character:
                                    <img id="characterImage" src={Pingu} className="mini-image" alt="characterImage" />
                                </div>
                            </div>
                            <div className="card-footer" >
                                <div id="pingu" className="card col-6 pic" onClick={this.handleState}>
                                    <img alt="pinguImage" src={require('./../imgs/Pingu.jpg')} />
                                </div>

                                <div id="panda" className="card col-6 pic" onClick={this.handleState2}>
                                    <img alt="pandaImage" src={require('./../imgs/Panda.jpg')} />
                                </div>

                                <br/>
                                <a href="./location"> <button className="btn btn-primary form-control">Send
                                </button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

    }
}

export default Character