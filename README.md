# About:

This application is a ChatApp by Erik Thiele and Hanna Wagner.


The user can Enter a Name, choose a Character and a Location.


In order to chat with each other open two browser windows, enter all the information (name ...) and let's go!

# Instalation:

git clone to your local folder


cd chatapp


yarn install


cd backend


yarn install


# Get it started:
**in backend folder:**


nodemon app.js


**in root folder:**


yarn start


# Our Steps:

create new React app

add Bootstrap

write basic chatapp with socket io

split views / do Routing

eject procet for own sass and webpack

added sass

added Google Maps API

added Google Geolocation